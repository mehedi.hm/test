import React from "react";
import { Link } from "react-router-dom";

export default function Welcome() {
  return (
    <>
      <div>Welcome To Hell!</div>
      <div>
        <ul>
          <li>
            <Link to="/post/65">Posts</Link>
          </li>
          <li>
            <Link to="/write-post">Write</Link>
          </li>
        </ul>
      </div>
    </>
  );
}
