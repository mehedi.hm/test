import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import "./App.css";
import welcome from "./components/welcome";
import posts from "./components/post";
import writepost from "./components/writepost";
import Welcome from "./components/welcome";
import Post from "./components/post";
import Writepost from "./components/writepost";

function App() {
  return (
    <Router>
      <Switch>
        <Redirect exact from="/" to="/muslimeli" />
        <Route exact path="/muslimeli" component={Welcome} />
        <Route path="/post/" component={Post} />
        <Route path="/write-post" component={Writepost} />
      </Switch>
    </Router>
  );
}

export default App;
